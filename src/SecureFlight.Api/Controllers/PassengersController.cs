﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PassengersController : ControllerBase
    {
        private readonly IService<Passenger> _personService;
        private readonly IRepository<PassengerFlight> _passengerFlightRepository;

        public PassengersController(IService<Passenger> personService, IRepository<PassengerFlight> passengerFlightRepository)
        {
            _personService = personService;
            _passengerFlightRepository = passengerFlightRepository;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Get()
        {
            var passengers = (await _personService.GetAllAsync()).Result
                .Select(x => new PassengerDataTransferObject
                {
                    Email = x.Email,
                    FirstName = x.FirstName,
                    Id = x.Id,
                    LastName = x.LastName
                });

            return Ok(passengers);
        }

        [HttpPut]
        [ProducesResponseType(typeof(IEnumerable<PassengerFlightTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Put(PassengerDataTransferObject passengerFlight)
        {
            var result = _passengerFlightRepository.Update(new PassengerFlight
            {
                FlightId = passengerFlight.
            });


            return Ok("Ok");
        }
    }
}
