﻿namespace SecureFlight.Api.Models
{
    public class PassengerFlightTransferObject
    {
        public string PassengerId { get; set; }

        public long FlightId { get; set; }
    }
}
